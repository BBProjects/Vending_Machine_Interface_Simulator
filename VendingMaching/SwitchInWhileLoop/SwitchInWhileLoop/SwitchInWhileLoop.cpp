//Brendan Burke and Chandler Roper
// SwitchInWhileLoop.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
		int choice;
		int cont=1;
		double coke, water, sprite, rootbeer;
		double lays, doritos, cheetos, fritos;
		double snickers, threeMuska, payday, fruitsna;
		double con;
		double change;
		double total=0;
		double cash;
		double MoMoney=0;
		
		cout << "SPECIAL SALE -- ALL ITEMS ONLY $1.25!!!" << endl;
		cout << endl;

		cout << "Cash:";
		cin >> cash;

		while (cont == 1){
		cout << "------------------------"<<endl;
		cout << "1 Coke\t\t";
		cout << "2 Water \t";
		cout << "3 Sprite\t\t";
		cout << "4 Root Beer \t" << endl;
		cout << "5 Snickers\t";
		cout << "6 Payday\t";//was 3muska
		cout << "7 Three Muskateers\t";//was payday--
		cout << "8 Fruit Snacks" << endl;
		cout << "9 Doritos\t";
		cout << "10 Lays\t\t";
		cout << "11 Cheetos\t\t";
		cout << "12 Fritos\t" << endl;
		cout << "------------------------"<<endl;

		cin >> choice;
		cout << endl;
		
			switch (choice){
			case 1:
				cout << "How many Cokes: ";
				cin >> coke;
				cout << endl;
				con = coke;
				coke *= 1.25;
				total += coke;
				change = cash - total;
				if (total > cash){
					cout << "Insufficient funds." << endl;
					cout << "Current change: " << change << endl;
					cout << "Please enter more money: ";

					cin >> MoMoney;
					cash += MoMoney;
					MoMoney += change;
					change = MoMoney;

					cout << endl;

					cout << "Current change: " << change << endl;
					cout << con << " Cokes received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
						if (cont == 1){
							break;
							}
				}
				
				else {
					cout << "Current change: " << change << endl;
					cout << con << " Cokes received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
				}
				break;

			case 2:
				cout << "How many waters";
				cin >> water;
				con = water;
				water *= 1.25;
				total += water;
				change = cash - total;
				if (total > cash){
					cout << "Insufficient funds." << endl;
					cout << "Current change: " << change << endl;
					cout << "Please enter more money: ";

					cin >> MoMoney;
					cash += MoMoney;
					MoMoney += change;
					change = MoMoney;

					cout << endl;

					cout << "Current change: " << change << endl;
					cout << con << " waters received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
					if (cont == 1){
						break;
					}
				}

				else {
					cout << "Current change: " << change << endl;
					cout << con << " waters received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
				}
				break;


			case 3:
				cout << "How many Sprites: ";
				cin >> sprite;
				con = sprite;
				sprite *= 1.25;
				total += sprite;
				change = cash - total;
				if (total > cash){
					cout << "Insufficient funds." << endl;
					cout << "Current change: " << change << endl;
					cout << "Please enter more money: ";

					cin >> MoMoney;
					cash += MoMoney;
					MoMoney += change;
					change = MoMoney;

					cout << endl;

					cout << "Current change: " << change << endl;
					cout << con << " Sprites received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
					if (cont == 1){
						break;
					}
				}

				else {
					cout << "Current change: " << change << endl;
					cout << con << " Sprites received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
				}
				break;

			case 4:
				cout << "How many Root Beers: ";
				cin >> rootbeer;
				con = rootbeer;
				rootbeer *= 1.25;
				total += rootbeer;
				change = cash - total;
				if (total > cash){
					cout << "Insufficient funds." << endl;
					cout << "Current change: " << change << endl;
					cout << "Please enter more money: ";

					cin >> MoMoney;
					cash += MoMoney;
					MoMoney += change;
					change = MoMoney;

					cout << endl;

					cout << "Current change: " << change << endl;
					cout << con << " Root Beers received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
					if (cont == 1){
						break;
					}
				}

				else {
					cout << "Current change: " << change << endl;
					cout << con << " Root Beers received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
				}
				break;

			case 5:
				cout << "How many Snickers: ";
				cin >> snickers;
				con = snickers;
				snickers *= 1.25;
				total += snickers;
				change = cash - total;
				if (total > cash){
					cout << "Insufficient funds." << endl;
					cout << "Current change: " << change << endl;
					cout << "Please enter more money: ";

					cin >> MoMoney;
					cash += MoMoney;
					MoMoney += change;
					change = MoMoney;

					cout << endl;

					cout << "Current change: " << change << endl;
					cout << con << " Snickers received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
					if (cont == 1){
						break;
					}
				}

				else {
					cout << "Current change: " << change << endl;
					cout << con << " Snickers received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
				}
				break;			

			case 6:
				cout << "How many Paydays: ";
				cin >> payday;
				con = payday;
				payday *= 1.25;
				total += payday;
				change = cash - total;
				if (total > cash){
					cout << "Insufficient funds." << endl;
					cout << "Current change: " << change << endl;
					cout << "Please enter more money: ";

					cin >> MoMoney;
					cash += MoMoney;
					MoMoney += change;
					change = MoMoney;

					cout << endl;

					cout << "Current change: " << change << endl;
					cout << con << " Paydays received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
					if (cont == 1){
						break;
					}
				}

				else {
					cout << "Current change: " << change << endl;
					cout << con << " Paydays received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
				}
				break;

			case 7:
				cout << "How many Three Muskateers: ";
				cin >> threeMuska;
				con = threeMuska;
				threeMuska *= 1.25;
				total += threeMuska;
				change = cash - total;
				if (total > cash){
					cout << "Insufficient funds." << endl;
					cout << "Current change: " << change << endl;
					cout << "Please enter more money: ";

					cin >> MoMoney;
					cash += MoMoney;
					MoMoney += change;
					change = MoMoney;

					cout << endl;

					cout << "Current change: " << change << endl;
					cout << con << " Three Muskateers received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
					if (cont == 1){
						break;
					}
				}

				else {
					cout << "Current change: " << change << endl;
					cout << con << " Three Muskateers received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
				}
				break;

			case 8:
				cout << "How many Fruit Snacks: ";
				cin >> fruitsna;
				con = fruitsna;
				fruitsna *= 1.25;
				total += fruitsna;
				change = cash - total;
				if (total > cash){
					cout << "Insufficient funds." << endl;
					cout << "Current change: " << change << endl;
					cout << "Please enter more money: ";

					cin >> MoMoney;
					cash += MoMoney;
					MoMoney += change;
					change = MoMoney;

					cout << endl;

					cout << "Current change: " << change << endl;
					cout << con << " Fruit Snacks received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
					if (cont == 1){
						break;
					}
				}

				else {
					cout << "Current change: " << change << endl;
					cout << con << " Fruit Snacks received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
				}
				break;


			case 9:
				cout << "How many Doritos: ";
				cin >> doritos;
				con = doritos;
				doritos *= 1.25;
				total += doritos;
				change = cash - total;
				if (total > cash){
					cout << "Insufficient funds." << endl;
					cout << "Current change: " << change << endl;
					cout << "Please enter more money: ";

					cin >> MoMoney;
					cash += MoMoney;
					MoMoney += change;
					change = MoMoney;

					cout << endl;

					cout << "Current change: " << change << endl;
					cout << con << " Doritos received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
					if (cont == 1){
						break;
					}
				}

				else {
					cout << "Current change: " << change << endl;
					cout << con << " Doritos received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
				}
				break;

			case 10:
				cout << "How many Lays: ";
				cin >> lays;
				con = lays;
				lays *= 1.25;
				total += lays;
				change = cash - total;
				if (total > cash){
					cout << "Insufficient funds." << endl;
					cout << "Current change: " << change << endl;
					cout << "Please enter more money: ";

					cin >> MoMoney;
					cash += MoMoney;
					MoMoney += change;
					change = MoMoney;

					cout << endl;

					cout << "Current change: " << change << endl;
					cout << con << " Lays received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
					if (cont == 1){
						break;
					}
				}

				else {
					cout << "Current change: " << change << endl;
					cout << con << " Lays received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
				}
				break;

			case 11:
				cout << "How many Cheetos: ";
				cin >> cheetos;
				con = cheetos;
				cheetos *= 1.25;
				total += cheetos;
				change = cash - total;
				if (total > cash){
					cout << "Insufficient funds." << endl;
					cout << "Current change: " << change << endl;
					cout << "Please enter more money: ";

					cin >> MoMoney;
					cash += MoMoney;
					MoMoney += change;
					change = MoMoney;

					cout << endl;

					cout << "Current change: " << change << endl;
					cout << con << " Cheetos received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
					if (cont == 1){
						break;
					}
				}

				else {
					cout << "Current change: " << change << endl;
					cout << con << " Cheetos received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
				}
				break;

			case 12:
				cout << "How many Fritos: ";
				cin >> fritos;
				con = fritos;
				fritos *= 1.25;
				total += fritos;
				change = cash - total;
				if (total > cash){
					cout << "Insufficient funds." << endl;
					cout << "Current change: " << change << endl;
					cout << "Please enter more money: ";

					cin >> MoMoney;
					cash += MoMoney;
					MoMoney += change;
					change = MoMoney;

					cout << endl;

					cout << "Current change: " << change << endl;
					cout << con << " Fritos received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
					if (cont == 1){
						break;
					}
				}

				else {
					cout << "Current change: " << change << endl;
					cout << con << " Fitos received" << endl;
					cout << "Continue? (1=YES, 2=NO)" << endl;
					cin >> cont;
					cout << endl;
				}
				break;


			default:
				cout << "you bad" << endl;
				continue;
			}

		}
	return 0;
}